
var numList = [];  //数字
var smallList = []; //小写字母
var bigList = [];//大写字母
var speList = ["_", "$", "#", "@"];  //特殊字符
var allList = [];//所有合法字符的集合
var norList = [];

for (var i = 0; i <= 9; i++) {
    numList.push(String(i));  // 转成字符串
}

for (var i = 97; i <= 122; i++) {
    var code = String.fromCharCode(i);
    // console.log(code);
    smallList.push(code);
}

for (var i = 65; i <= 90; i++) {
    var code = String.fromCharCode(i);
    // console.log(code);
    bigList.push(code);
}

allList = numList.concat(smallList, bigList, speList);
console.log(allList);

norList = numList.concat(smallList, bigList);

function randCode() {
    var str = "";
    for (var i = 0; i < 4; i++) {
        var index = Math.floor(Math.random() * norList.length);
        var char = norList[index];
        if (indexOf(str, char) == -1) {
            str += norList[index];
        } else {
            i--;
        }

    }
    return str;
}

function indexOf(list, char) {
    var index = -1;
    for (var i in list) {
        var item = list[i];
        if (item === char) {
            index = i;
            break;
        }
    }
    return index;




    // var flag = false;
    // for (var item of list) {
    //     if (item === char) {
    //         flag = true;
    //         break;
    //     }
    // }
    // return flag;
}


// 判断用户名是否合法  合法就返回 true  不合法返回  false
function judgeUser(user) {
    // var user = userInp.value;
    // [true,"用户名已存在..."]
    // var obj = {
    //     status: true,   // status 代表状态   true  合法  ,  false  不合法
    //     detail: "√"     // detail 代表提示信息
    // }

    var obj = {
        status: false,
        detail: ""
    }

    if (user) {

        if (user.length >= 6 && user.length <= 12) {  //用户名长度判断
            var firstChar = user.charAt(0);
            if (numList.indexOf(firstChar) == -1) { //用户名首字符判断

                var flag = true;
                for (var char of user) {  //遍历user   取到每一个字符 char
                    // console.log(char); 
                    if (allList.indexOf(char) == -1) {
                        flag = false;
                        break;
                    }

                }

                if (flag) {
                    if (userList.indexOf(user) == -1) {
                        // userList.push(user);
                        // user_span.innerText = "√";
                        // user_span.style.color = "green";

                        obj.status = true;
                        obj.detail = "√";
                        return obj;

                    } else {
                        // user_span.innerText = "用户名已存在...";
                        // user_span.style.color = "red";
                        obj.status = false;
                        obj.detail = "用户名已存在...";
                        return obj;
                    }

                } else {
                    // user_span.innerText = "用户名含有非法字符...";
                    // user_span.style.color = "red";

                    obj.status = false;
                    obj.detail = "用户名含有非法字符...";
                    return obj;
                }

            } else {
                // user_span.innerText = "用户名不能以数字开头...";
                // user_span.style.color = "red";

                obj.status = false;
                obj.detail = "用户名不能以数字开头...";
                return obj;
            }


        } else {
            // user_span.innerText = "用户名需要在6-12位之间...";
            // user_span.style.color = "red";

            obj.status = false;
            obj.detail = "用户名需要在6-12位之间...";
            return obj;
        }

    } else {
        // user_span.innerText = "请输入用户名...";
        // user_span.style.color = "red";
        // return false;
        obj.status = false;
        obj.detail = "请输入用户名...";
        return obj;
    }

}

