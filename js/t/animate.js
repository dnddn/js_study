// 多物体运动  需要将计时器绑在元素上
function animate(ele, attr, end, aniType, fn) {

    var start = parseFloat(getStyleAttr(ele, attr));//0

    var end = parseFloat(end);
    var speed = 0;
    clearInterval(ele.timer);
    ele.timer = setInterval(function () {
        if (aniType == "linear") {
            speed = (end - start) / 20;
        } else if (aniType == "ease-in") {
            speed = (end - start > 0 ? speed + 3 : speed - 3);
        } else if (aniType == "ease-out") {
            speed = (end - parseFloat(getStyleAttr(ele, attr))) / 10;
            speed = speed > 0 ? Math.ceil(speed) : Math.floor(speed);
        }
        // console.log(speed);
        ele.style[attr] = parseFloat(getStyleAttr(ele, attr)) + speed + "px";  //更新ele的位置
        if (speed > 0) {
            if (parseFloat(getStyleAttr(ele, attr)) >= end) {
                clearInterval(ele.timer);
                ele.style[attr] = end + "px";
                if (fn) {
                    fn();
                }
            }
        } else {
            if (parseFloat(getStyleAttr(ele, attr)) <= end) {
                clearInterval(ele.timer);
                ele.style[attr] = end + "px";
                if (fn) {
                    fn();
                }
            }
        }

    }, 30)
}


function getStyleAttr(ele, attr) {
    if (window.getComputedStyle) {
        return window.getComputedStyle(ele)[attr];
    } else {
        return ele.currentStyle[attr];
    }
}