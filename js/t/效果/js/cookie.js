function getCookie(key) {
    //"username=lili; userpwd=321"
    var str = document.cookie;
    if (str) {
        var cookieList = str.split("; ");//["username=lili","userpwd=321"];
        for (var i = 0; i < cookieList.length; i++) {
            var cookie = cookieList[i].split("=");//
            var cookieKey = decodeURI(cookie[0]);
            if (cookieKey == key) {//找到指定的key
                return decodeURI(cookie[1]);
            }
        }
        return "";
    } else {
        return "";
    }

}

/**
 * cookie的增删改
 * @param key
 * @param value
 * @param days
 */
function setCookie(key, value, days) { //新增  修改  删除
    var date = new Date();
    date.setDate(date.getDate() + days);//设置过期时间
    document.cookie = encodeURI(key) + "=" + encodeURI(value) + ";expires=" + date+";path=/"+";domain=localhost";
}