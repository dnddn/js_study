function next(ele) {
    if(ele.nextElementSibling){
        return  ele.nextElementSibling;
    }else{
        return  ele.nextSibling;
    }

}
function prev(ele) {
    if (ele.previousElementSibling) {
        return ele.previousElementSibling;
    } else {
        return ele.previousSibling;
    }
}

function getStyleAttr(obj, attr) {
    if (window.getComputedStyle) {
        return window.getComputedStyle(obj)[attr];
    } else {
        return obj.currentStyle[attr];
    }
}
function $(str, parent) {
    if (parent) {
        parent = parent;
    } else {
        parent = document;
    }
    if (str.indexOf(" ") != -1) {
        var list = str.split(" ");//["ul","li","span"];
        for (var i = 0; i < list.length; i++) {
            var item = list[i];//找到里面的每一个元素  ul   li span
            if (i == list.length - 1) {
                parent = $(item, parent);
            } else {
                if (item.charAt(0) == "#") {
                    parent = $(item, parent);
                } else {
                    parent = $(item, parent)[0];//精华  $("ul",document)   parent 就变成了第一个ul
                    // $("li",ul)
                    //parent 变成了 li  $("span", li)
                }
            }
        }
        return  parent;
    } else {
        var firstCode = str.charAt(0);//"#","."
        switch (firstCode) {
            case "#":
                return parent.getElementById(str.substring(1));
            case ".":
                return parent.getElementsByClassName(str.substring(1));
            default:
                return parent.getElementsByTagName(str);
        }
    }
}