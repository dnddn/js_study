function GetEle(ele) {
    if (typeof  ele == "object") {
        this.eles = [ele];
    } else {
        this.eles = document.querySelectorAll(ele);
        this.setIndex();
    }
}
GetEle.prototype.setIndex = function () {
    this.each(function (item, index) {
        item.index = index;
    })
}

GetEle.prototype.each = function (fn) {
    for (var i = 0; i < this.eles.length; i++) {
        var item = this.eles[i];
        fn(item, i);
    }

}
GetEle.prototype.hide = function () {
    this.each(function (item) {
      //  this.style.display = "none";
        item.style.display = "none";
    })
    return this;
}
GetEle.prototype.show = function () {
    this.each(function (item) {
        item.style.display = "block";
    })
    return this;
}
GetEle.prototype.html = function (content) {
    if (arguments.length == 1) {
        this.each(function (item) {
            item.innerHTML = content;//这里使用arguments就会炸掉
        })
        return this;
    } else {
        return this.eles[0].innerHTML;
    }
}
GetEle.prototype.height = function (content) {
    if (arguments.length == 1) {
        this.each(function (item) {
            item.style.height = content + "px";
        })
    } else {
        //  this.eles[0].style.height;
        return parseInt(this.getStyleAttr(this.eles[0], "height"));
    }
}
GetEle.prototype.getStyleAttr = function (obj, attr) {
    if (window.getComputedStyle) {
        return window.getComputedStyle(obj)[attr];
    } else {
        return obj.currentStyle[attr];
    }
}
GetEle.prototype.css = function () {
    if (arguments.length == 2) {
        var attr = arguments[0];
        var attrValue = arguments[1];
        this.each(function (item) {
            //item.style[arguments[0]] = arguments[1];
            item.style[attr] = attrValue;
        })
    } else if (arguments.length == 1) {

        if (typeof  arguments[0] == "object") {
            var obj = arguments[0];
            for (var attr  in obj) {
                this.each(function (item) {
                    item.style[attr] = obj[attr];
                })
            }
        }
        if (typeof  arguments[0] == "string") {
            return this.getStyleAttr(this.eles[0], arguments[0]);
        }
    }
    return this;
}
GetEle.prototype.click = function (fn) {
    this.each(function (item) {
        item.onclick = fn;
    })

}
GetEle.prototype.on = function (type, fn) {
    this.each(function (item) {
        item.addEventListener(type, fn);
    })
}
GetEle.prototype.eq = function (index) {
    this.defaultEles = this.eles;//[1,2,3,4,5]
    this.eles = [this.eles[index]];//[3]
    return this;
}
//[1,2,3,4,5]
GetEle.prototype.siblings = function () {
    var temp = [];
    for (var i = 0; i < this.defaultEles.length; i++) {
        if (this.defaultEles[i] != this.eles[0]) {
            temp.push(this.defaultEles[i]);
        }
    }
    this.eles = temp;
    return this;
}
GetEle.prototype.index = function () {
    return this.eles[0].index;
}
function $(ele) {
    return new GetEle(ele);
}
//   $("#nav li").click(function () {
   //     var index = $(this).index();
 //       $("#nav li").eq(index).css("background", "red").siblings().css("background", "black");
  //      $("#content li").eq(index).show().siblings().hide();
  //  })
