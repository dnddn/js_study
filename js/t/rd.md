
事件对象event的属性
    ctrlKey , altKey , shiftKey , metaKey   当事件被触发时  对应按键是否被按下   按下返回true ,没有按下返回false
    button  返回事件触发时鼠标的按键   0：左键   1：中键(滑轮)  2：右键
    buttons 

    clientX  可视坐标  (相对于窗口的最左边轴坐标距离)   不包含滚动条
    clientY  可视坐标  (相对于窗口的最上边轴坐标距离)

    pageX    实际坐标X  (相对于文档的最左边轴坐标距离)  包含滚动条
    pageY    实际坐标Y  (相对于文档的最左边轴坐标距离)


    offsetX  相对于事件目标最左边的偏移   (注意：一般有父子关系的元素不使用这个属性)
    offsetY  相对于事件目标最上边的偏移
    <!-- 注意：一般有父子关系的元素时应该这么算 -->
    e.pageX - box.offsetLeft;
    e.pageY - box.offsetTop


    screenX   距离屏幕最左边轴坐标距离
    screenY   距离屏幕最上边轴坐标距离   

